
/****** Object:  Table [dbo].[假期]    Script Date: 2022/7/16 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[假期](
	[结束时间] [datetime] NULL,
	[开始时间] [datetime] NULL,
	[假期ID] [char](32) NOT NULL,
	[假期类型ID] [char](32) NULL,
 CONSTRAINT [PK_假期] PRIMARY KEY CLUSTERED 
(
	[假期ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[假期类型]    Script Date: 2022/7/16 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[假期类型](
	[名称] [varchar](50) NULL,
	[假期类型ID] [char](32) NOT NULL,
 CONSTRAINT [PK_假期类型] PRIMARY KEY CLUSTERED 
(
	[假期类型ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[排期]    Script Date: 2022/7/16 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[排期](
	[结束时间] [datetime] NULL,
	[开始时间] [datetime] NULL,
	[排期ID] [char](32) NOT NULL,
	[任务ID] [char](32) NULL,
 CONSTRAINT [PK_排期] PRIMARY KEY CLUSTERED 
(
	[排期ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[任务]    Script Date: 2022/7/16 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[任务](
	[概述] [varchar](max) NULL,
	[计划结束时间] [datetime] NULL,
	[计划开始时间] [datetime] NULL,
	[可否改期] [int] NULL,
	[是否已排期] [int] NULL,
	[预计耗时] [float] NULL,
	[任务ID] [char](32) NOT NULL,
	[任务类型ID] [char](32) NULL,
	[依赖任务] [char](32) NULL,
	[前一任务] [char](32) NULL,
 CONSTRAINT [PK_任务] PRIMARY KEY CLUSTERED 
(
	[任务ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[任务类型]    Script Date: 2022/7/16 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[任务类型](
	[名称] [varchar](50) NULL,
	[任务类型ID] [char](32) NOT NULL,
	[前一高优先级任务类型] [char](32) NULL,
 CONSTRAINT [PK_任务类型] PRIMARY KEY CLUSTERED 
(
	[任务类型ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[日时间段]    Script Date: 2022/7/16 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[日时间段](
	[日时间段ID] [char](32) NOT NULL,
	[设定日时间段ID] [char](32) NULL,
	[开始时辰] [char](32) NULL,
	[结束时辰] [char](32) NULL,
 CONSTRAINT [PK_日时间段] PRIMARY KEY CLUSTERED 
(
	[日时间段ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[日时间段类型]    Script Date: 2022/7/16 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[日时间段类型](
	[名称] [varchar](50) NULL,
	[日时间段类型ID] [char](32) NOT NULL,
 CONSTRAINT [PK_日时间段类型] PRIMARY KEY CLUSTERED 
(
	[日时间段类型ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[设定目标时长]    Script Date: 2022/7/16 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[设定目标时长](
	[目标时长] [float] NULL,
	[设定目标时长ID] [char](32) NOT NULL,
	[任务类型ID] [char](32) NOT NULL,
 CONSTRAINT [PK_设定目标时长] PRIMARY KEY CLUSTERED 
(
	[设定目标时长ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[设定日时间段]    Script Date: 2022/7/16 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[设定日时间段](
	[设定日时间段ID] [char](32) NOT NULL,
	[日时间段类型ID] [char](32) NOT NULL,
	[自然日类型ID] [char](32) NOT NULL,
 CONSTRAINT [PK_设定日时间段] PRIMARY KEY CLUSTERED 
(
	[设定日时间段ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[时辰]    Script Date: 2022/7/16 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[时辰](
	[分] [int] NULL,
	[秒] [int] NULL,
	[时] [int] NULL,
	[时辰ID] [char](32) NOT NULL,
 CONSTRAINT [PK_时辰] PRIMARY KEY CLUSTERED 
(
	[时辰ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[时间段适合投入]    Script Date: 2022/7/16 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[时间段适合投入](
	[日时间段类型ID] [char](32) NULL,
	[任务类型ID] [char](32) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[自然日]    Script Date: 2022/7/16 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[自然日](
	[日期] [datetime] NULL,
	[自然日ID] [char](32) NOT NULL,
	[假期ID] [char](32) NULL,
	[自然日类型ID] [char](32) NULL,
 CONSTRAINT [PK_自然日] PRIMARY KEY CLUSTERED 
(
	[自然日ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[自然日类型]    Script Date: 2022/7/16 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[自然日类型](
	[名称] [varchar](50) NULL,
	[自然日类型ID] [char](32) NOT NULL,
	[设定目标时长ID] [char](32) NOT NULL,
 CONSTRAINT [PK_自然日类型] PRIMARY KEY CLUSTERED 
(
	[自然日类型ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[自然日适合投入]    Script Date: 2022/7/16 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[自然日适合投入](
	[自然日类型ID] [char](32) NULL,
	[任务类型ID] [char](32) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[任务] ADD  CONSTRAINT [DF__任务__可否改期__09A971A2]  DEFAULT ((1)) FOR [可否改期]
GO
ALTER TABLE [dbo].[任务] ADD  CONSTRAINT [DF__任务__是否已排期__0A9D95DB]  DEFAULT ((0)) FOR [是否已排期]
GO
ALTER TABLE [dbo].[假期]  WITH CHECK ADD  CONSTRAINT [FK_假期_假期类型] FOREIGN KEY([假期类型ID])
REFERENCES [dbo].[假期类型] ([假期类型ID])
GO
ALTER TABLE [dbo].[假期] CHECK CONSTRAINT [FK_假期_假期类型]
GO
ALTER TABLE [dbo].[排期]  WITH CHECK ADD  CONSTRAINT [FK_排期_任务] FOREIGN KEY([任务ID])
REFERENCES [dbo].[任务] ([任务ID])
GO
ALTER TABLE [dbo].[排期] CHECK CONSTRAINT [FK_排期_任务]
GO
ALTER TABLE [dbo].[任务]  WITH CHECK ADD  CONSTRAINT [FK_任务_被依赖] FOREIGN KEY([依赖任务])
REFERENCES [dbo].[任务] ([任务ID])
GO
ALTER TABLE [dbo].[任务] CHECK CONSTRAINT [FK_任务_被依赖]
GO
ALTER TABLE [dbo].[任务]  WITH CHECK ADD  CONSTRAINT [FK_任务_后] FOREIGN KEY([前一任务])
REFERENCES [dbo].[任务] ([任务ID])
GO
ALTER TABLE [dbo].[任务] CHECK CONSTRAINT [FK_任务_后]
GO
ALTER TABLE [dbo].[任务]  WITH CHECK ADD  CONSTRAINT [FK_任务_任务类型] FOREIGN KEY([任务类型ID])
REFERENCES [dbo].[任务类型] ([任务类型ID])
GO
ALTER TABLE [dbo].[任务] CHECK CONSTRAINT [FK_任务_任务类型]
GO
ALTER TABLE [dbo].[任务类型]  WITH CHECK ADD  CONSTRAINT [FK_任务类型_优先级] FOREIGN KEY([前一高优先级任务类型])
REFERENCES [dbo].[任务类型] ([任务类型ID])
GO
ALTER TABLE [dbo].[任务类型] CHECK CONSTRAINT [FK_任务类型_优先级]
GO
ALTER TABLE [dbo].[日时间段]  WITH CHECK ADD  CONSTRAINT [FK_日时间段_设定日时间段] FOREIGN KEY([设定日时间段ID])
REFERENCES [dbo].[设定日时间段] ([设定日时间段ID])
GO
ALTER TABLE [dbo].[日时间段] CHECK CONSTRAINT [FK_日时间段_设定日时间段]
GO
ALTER TABLE [dbo].[日时间段]  WITH CHECK ADD  CONSTRAINT [FK_日时间段_时辰] FOREIGN KEY([开始时辰])
REFERENCES [dbo].[时辰] ([时辰ID])
GO
ALTER TABLE [dbo].[日时间段] CHECK CONSTRAINT [FK_日时间段_时辰]
GO
ALTER TABLE [dbo].[设定目标时长]  WITH CHECK ADD  CONSTRAINT [FK_设定目标时长_任务类型] FOREIGN KEY([任务类型ID])
REFERENCES [dbo].[任务类型] ([任务类型ID])
GO
ALTER TABLE [dbo].[设定目标时长] CHECK CONSTRAINT [FK_设定目标时长_任务类型]
GO
ALTER TABLE [dbo].[设定日时间段]  WITH CHECK ADD  CONSTRAINT [FK_设定日时间段_日时间段类型] FOREIGN KEY([日时间段类型ID])
REFERENCES [dbo].[日时间段类型] ([日时间段类型ID])
GO
ALTER TABLE [dbo].[设定日时间段] CHECK CONSTRAINT [FK_设定日时间段_日时间段类型]
GO
ALTER TABLE [dbo].[设定日时间段]  WITH CHECK ADD  CONSTRAINT [FK_设定日时间段_自然日类型] FOREIGN KEY([自然日类型ID])
REFERENCES [dbo].[自然日类型] ([自然日类型ID])
GO
ALTER TABLE [dbo].[设定日时间段] CHECK CONSTRAINT [FK_设定日时间段_自然日类型]
GO
ALTER TABLE [dbo].[时间段适合投入]  WITH CHECK ADD  CONSTRAINT [FK_时间段适合投入_任务类型] FOREIGN KEY([任务类型ID])
REFERENCES [dbo].[任务类型] ([任务类型ID])
GO
ALTER TABLE [dbo].[时间段适合投入] CHECK CONSTRAINT [FK_时间段适合投入_任务类型]
GO
ALTER TABLE [dbo].[时间段适合投入]  WITH CHECK ADD  CONSTRAINT [FK_时间段适合投入_日时间段类型] FOREIGN KEY([日时间段类型ID])
REFERENCES [dbo].[日时间段类型] ([日时间段类型ID])
GO
ALTER TABLE [dbo].[时间段适合投入] CHECK CONSTRAINT [FK_时间段适合投入_日时间段类型]
GO
ALTER TABLE [dbo].[自然日]  WITH CHECK ADD  CONSTRAINT [FK_自然日_属于] FOREIGN KEY([假期ID])
REFERENCES [dbo].[假期] ([假期ID])
GO
ALTER TABLE [dbo].[自然日] CHECK CONSTRAINT [FK_自然日_属于]
GO
ALTER TABLE [dbo].[自然日]  WITH CHECK ADD  CONSTRAINT [FK_自然日_自然日类型] FOREIGN KEY([自然日类型ID])
REFERENCES [dbo].[自然日类型] ([自然日类型ID])
GO
ALTER TABLE [dbo].[自然日] CHECK CONSTRAINT [FK_自然日_自然日类型]
GO
ALTER TABLE [dbo].[自然日类型]  WITH CHECK ADD  CONSTRAINT [FK_自然日类型_设定目标时长] FOREIGN KEY([设定目标时长ID])
REFERENCES [dbo].[设定目标时长] ([设定目标时长ID])
GO
ALTER TABLE [dbo].[自然日类型] CHECK CONSTRAINT [FK_自然日类型_设定目标时长]
GO
ALTER TABLE [dbo].[自然日适合投入]  WITH CHECK ADD  CONSTRAINT [FK_自然日适合投入_任务类型] FOREIGN KEY([任务类型ID])
REFERENCES [dbo].[任务类型] ([任务类型ID])
GO
ALTER TABLE [dbo].[自然日适合投入] CHECK CONSTRAINT [FK_自然日适合投入_任务类型]
GO
ALTER TABLE [dbo].[自然日适合投入]  WITH CHECK ADD  CONSTRAINT [FK_自然日适合投入_自然日类型] FOREIGN KEY([自然日类型ID])
REFERENCES [dbo].[自然日类型] ([自然日类型ID])
GO
ALTER TABLE [dbo].[自然日适合投入] CHECK CONSTRAINT [FK_自然日适合投入_自然日类型]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1是0否' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'任务', @level2type=N'COLUMN',@level2name=N'可否改期'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1是0否' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'任务', @level2type=N'COLUMN',@level2name=N'是否已排期'
GO
